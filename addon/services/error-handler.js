import Service from '@ember/service';
import {inject} from '@ember/service';
import {isPresent} from '@ember/utils';
import {camelize} from '@ember/string';

export default Service.extend({
    notifications: inject(),
    session: inject(),
    store: inject(),
    router: inject(),

    processError(error, changeset = null) {
        if (error.status == 401) {
            this.get('store').unloadAll('merchant');

            if(this.get('session.isAuthenticated')) {
                this.get('session').invalidate();
            } else {
                this.get('router').transitionTo('login');
            }
        }

        // Add any changeset errors returned from API.
        if(changeset && error.source) {
            let pointer = error.source.pointer.split('/');
            let attributeName = camelize(pointer[pointer.length - 1]);
            changeset.addError(attributeName, error.detail);
        }

        let errorText = 'An unexpected error occurred';

        if(error.detail) {
            errorText = error.detail;
        } else if(error.message) {
            errorText = error.message;
        }

        return `<div class="item"><i class="warning circle icon"></i><div class="content">${errorText}</div></div>`;
    },

    handleErrors(e, changeset = null) {
        // Display errors to user.
        if(isPresent(e.text)) {
            e = JSON.parse(e.text);
        }

        if(e.body || isPresent(e.errors) || (isPresent(e.payload) && isPresent(e.payload.errors))) {
            let errors = [];

            if(isPresent(e.errors)) {
                errors = e.errors;
            } else if(e.payload && isPresent(e.payload.errors)) {
                errors = e.payload.errors;
            } else if(e.body) {
                // These errors occur when calling the "upload" method on a file selected with the ember-file-upload
                // add-on.
                if(e.body.errors) {
                    // This is a system error that contains revealing information about our environment so don't show it.
                    if(Array.isArray(e.body.errors) && e.body.errors[0] && e.body.errors[0].status >= 500) {
                        errors = '';
                    } else {
                        errors = e.body.errors;
                    }
                } else {
                    for(let key in e.body) {
                        e.body[key].forEach(errorMessage => {
                            let error = {
                                message: errorMessage
                            };

                            errors.push(error);
                        });
                    }
                }
            }

            let notifyText = '<div class="ui one column grid"><div class="column"><h3 class="ui header">One or more errors occurred:</h3></div><div class="column"><div class="ui white list">';

            // Clear all changeset errors
            if(changeset) {
                changeset.rollbackInvalid();
            }

            if(Array.isArray(errors)) {
                errors.forEach((error) => {
                    notifyText += this.processError(error, changeset);
                });
            } else {
                notifyText += this.processError(errors, changeset);
            }

            notifyText += '</div></div></div>';

            // Only show if the application is online otherwise the user is swamped with failed API request notifications.
            if(navigator.onLine) {
                this.get('notifications').error(notifyText, {
                    htmlContent: true,
                    cssClasses: 'notification-message'
                });
            }
        } else if(isPresent(e.message)) {
            // Only show if the application is online otherwise the user is swamped with failed API request notifications.
            if(navigator.onLine) {
                this.get('notifications').error(e.message, {
                    cssClasses: 'notification-message'
                });
            }

            if(e.message === '') {
                this.get('router').transitionTo('login');
            }

            if(this.get('session.isAuthenticated')) {
                // Log long description of exception to database.
                let log = this.get('store').createRecord('log', {
                    logLevelId: 5,
                    logTypeId: 3,
                    userId: this.get('session.data.userId'),
                    message: e.message
                });

                log.save();
            }
        }
    },

    handleValidationErrors(errorMessages) {
        let notifyText = '<div class="ui one column grid"><div class="column"><h3 class="ui header">One or more errors occurred:</h3></div><div class="column"><div class="ui white list">';

        errorMessages.forEach((errorMessage) => {
            notifyText += `<div class="item"><i class="warning circle icon"></i><div class="content">${errorMessage}</div></div>`;
        });

        notifyText += '</div></div></div>';

        // Only show if the application is online otherwise the user is swamped with failed API request notifications.
        if(navigator.onLine) {
            this.get('notifications').error(notifyText, {
                htmlContent: true,
                cssClasses: 'notification-message'
            });
        }
    }
});
