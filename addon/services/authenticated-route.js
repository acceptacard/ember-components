import Service, {inject} from '@ember/service';

export default Service.extend({

    session: inject(),
    router: inject(),

    protect(transition) {
        if(!this.get('session.isAuthenticated')) {
            this.get('router').transitionTo('login');
            this.set('session.attemptedTransition', transition);
            return false;
        }

        return true;
    }
});
