import Service from '@ember/service';
import {inject} from '@ember/service';
import {task} from 'ember-concurrency';
import {storageFor} from 'ember-local-storage';
import {isPresent} from '@ember/utils';
import {getOwner} from '@ember/application';
import moment from 'moment';

const HTTP_METHOD_GET = 'GET';
const LOCALE_EN = 'en';

export default Service.extend({

    errorHandler: inject(),
    store: inject(),
    session: inject(),
    router: inject(),
    ajax: inject(),
    ajaxConfig: inject(),
    config: inject(),
    localStorage: storageFor('user-settings'),

    partner: null,
    newVersionCheckDate: moment(),

    taskGetUser: task(function * (userId) {
        try {
            let user = yield this.get('store').findRecord('user', userId);

            this.setProperties({
                id: user.get('id'),
                name: user.get('name'),
                email: user.get('email'),
                traderId: user.get('traderId'),
                roles: user.get('roles'),
                permissions: user.get('permissions'),
            });

            if(this.get('config.modulePrefix') !== 'admin-secure-operations-com') {
                yield this.get('taskGetPartners').perform();
            }

        } catch(e) {
            this.get('errorHandler').handleErrors(e);
        }
    }).drop(),

    taskGetPartners: task(function * ()  {
        try {
            if(this.get('session.isAuthenticated')) {
                let partners = yield this.get('store').findAll('partner');
                let partner = null;

                if(this.get('roles').includes('admin')) {
                    // Admin user so use default Paya branding.
                    partner = partners.findBy('id', 'DEFAULT');
                } else {
                    partner = partners.get('firstObject');
                }

                if(isPresent(partner.get('branding'))) {
                    this.set('localStorage.brandingPrimaryColour', partner.get('branding.primaryColour'));
                    this.set('localStorage.brandingSecondaryColour', partner.get('branding.secondaryColour'));
                    this.set('localStorage.primaryTextColour', partner.get('branding.primaryTextColour'));
                    this.set('localStorage.secondaryTextColour', partner.get('branding.secondaryTextColour'));

                    if(partner.get('iso')) {
                        this.set('localStorage.headerLogo', partner.get('branding.isoHeaderLogo'));
                    } else {
                        this.set('localStorage.headerLogo', partner.get('branding.headerLogo'));
                    }

                    this.set('localStorage.headerBackgroundColour', partner.get('branding.headerBackgroundColor'));
                    this.set('localStorage.poweredByPcs', partner.get('branding.poweredByPcs'));
                    this.set('localStorage.readonlyForeground', partner.get('branding.readonlyForeground'));
                    this.set('localStorage.readonlyBackground', partner.get('branding.readonlyBackground'));
                    this.set('localStorage.isBrandingSet', true);
                }

                this.set('partner', partner);
            }
        } catch(e) {
            this.get('errorHandler').handleErrors(e);
        }

    }).drop(),

    taskGetVersions: task(function * (currentVersion, portal = 'admin') {
        try {
            let versions = yield this.get('store').query('version', {
                filter: {
                    application: portal
                }
            });

            if (isPresent(versions.get('firstObject')) && versions.get('firstObject.version') != currentVersion) {
                this.setProperties({
                    showUpdateNag: true,
                    newVersion: versions.get('firstObject.version'),
                    newVersionCheckDate: moment()
                });
            } else {
                this.setProperties({
                    showUpdateNag: false,
                    newVersion: null,
                    newVersionCheckDate: moment()
                });
            }

        } catch(e) {
            this.get('errorHandler').handleErrors(e);
        }
    }).drop(),
});
