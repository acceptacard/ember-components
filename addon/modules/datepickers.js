import { computed } from '@ember/object';
import { DEFAULT_DATE_FORMAT, DEFAULT_FLATPICKR_DATE_FORMAT, DATE_TODAY } from 'ember-components/modules/datetime';
import moment from 'moment';

// ---------- CONSTANTS ----------
export const defaultListFromDate = moment().subtract(1, 'month').format(DEFAULT_DATE_FORMAT);
export const defaultListToDate = moment().format(DEFAULT_DATE_FORMAT);

export const last30DaysListFromDate = moment().subtract(30, 'day').format(DEFAULT_DATE_FORMAT);
export const last30DaysListToDate = moment().format(DEFAULT_DATE_FORMAT);

export const lastSixMonthsListFromDate = moment().subtract(6, 'month').format(DEFAULT_DATE_FORMAT);
export const lastSixMonthsListToDate = moment().format(DEFAULT_DATE_FORMAT);

export const lastThreeCompleteMonthsFromDate = moment().subtract(3, 'month').startOf('month').format(DEFAULT_DATE_FORMAT);
export const lastThreeCompleteMonthsToDate = moment().subtract(1, 'month').endOf('month').format(DEFAULT_DATE_FORMAT);

export const DOB = {
    format: DEFAULT_FLATPICKR_DATE_FORMAT,
    minDate: moment().subtract(100, 'years').format(DEFAULT_DATE_FORMAT),
    maxDate: moment().subtract(18, 'years').subtract(1, 'day').format(DEFAULT_DATE_FORMAT),
};

export const LAST_HUNDRED_YEARS = {
    format: DEFAULT_FLATPICKR_DATE_FORMAT,
    minDate: moment().subtract(100, 'years').format(DEFAULT_DATE_FORMAT),
    maxDate: moment().subtract(1, 'days').format(DEFAULT_DATE_FORMAT),
};

export const IN_FUTURE = {
    format: DEFAULT_FLATPICKR_DATE_FORMAT,
    minDate: moment().add(1, 'days').format(DEFAULT_DATE_FORMAT),
    maxDate: moment().add(100, 'years').format(DEFAULT_DATE_FORMAT),
};

export const TODAY_ONWARDS = {
    format: DEFAULT_FLATPICKR_DATE_FORMAT,
    minDate: moment().format(DEFAULT_DATE_FORMAT),
    maxDate: moment().add(100, 'years').format(DEFAULT_DATE_FORMAT),
};

export const PLUS_MINUS_ONE_YEAR = {
    format: DEFAULT_FLATPICKR_DATE_FORMAT,
    minDate: moment().subtract(1, 'years').format(DEFAULT_DATE_FORMAT),
    maxDate: moment().add(1, 'years').format(DEFAULT_DATE_FORMAT),
    defaultDate: DATE_TODAY.toDate(),
};

export const NEXT_TEN_YEARS = {
    format: DEFAULT_FLATPICKR_DATE_FORMAT,
    minDate: moment().format(DEFAULT_DATE_FORMAT),
    maxDate: moment().add(10, 'years').format(DEFAULT_DATE_FORMAT),
};

export const TODAY_AND_IN_THE_PAST = {
    format: DEFAULT_FLATPICKR_DATE_FORMAT,
    minDate: moment().subtract(100, 'years').format(DEFAULT_DATE_FORMAT),
    maxDate: moment().format(DEFAULT_DATE_FORMAT),
};

export const ONE_YEAR_IN_THE_PAST_TO_THREE_YEARS_IN_THE_FUTURE = {
    format: DEFAULT_FLATPICKR_DATE_FORMAT,
    minDate: moment().subtract(1, 'years').format(DEFAULT_DATE_FORMAT),
    maxDate: moment().add(3, 'years').format(DEFAULT_DATE_FORMAT),
};

export const ONE_YEAR_IN_THE_PAST_TO_TODAY = {
    format: DEFAULT_FLATPICKR_DATE_FORMAT,
    minDate: moment().subtract(1, 'years').format(DEFAULT_DATE_FORMAT),
    maxDate: moment().format(DEFAULT_DATE_FORMAT),
};

export const NOW_TO_ONE_YEAR_IN_THE_FUTURE = {
    format: DEFAULT_FLATPICKR_DATE_FORMAT,
    minDate: moment().format(DEFAULT_DATE_FORMAT),
    maxDate: moment().add(1, 'years').format(DEFAULT_DATE_FORMAT),
};


// ---------- COMPUTED PROPERTIES ----------
export let lastTenYearsFrom = computed('toDate', function(){
    return {
        format: DEFAULT_FLATPICKR_DATE_FORMAT,
        minDate: moment().subtract(10, 'years').format(DEFAULT_DATE_FORMAT),
        maxDate: moment(this.get('toDate'), DEFAULT_DATE_FORMAT).format(DEFAULT_DATE_FORMAT),
    };
});

export let lastTenYearsTo = computed('fromDate', function() {
    return {
        format: DEFAULT_FLATPICKR_DATE_FORMAT,
        minDate: moment(this.get('fromDate'), DEFAULT_DATE_FORMAT).format(DEFAULT_DATE_FORMAT),
        maxDate: moment().format(DEFAULT_DATE_FORMAT)
    };
});