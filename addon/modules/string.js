import { computed } from '@ember/object';

export let underscoredRouteName = computed('router', function() {
    let routeName = this.get('router.currentRouteName');
    return routeName.replace(/\./g, '_');
});

export function replaceString(originalString, stringToReplaceRegex, replacementString, flags = 'gi') {
    return originalString.replace(new RegExp(stringToReplaceRegex, flags), replacementString);
}

export function formatSortCode(str) {
    return str.match(new RegExp(`.{1,2}`, 'g')).join('-');
}

export const YES = 'YES';
export const NO = 'NO';

export const Y = 'y';
export const N = 'n';