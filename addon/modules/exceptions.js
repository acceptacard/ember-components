const MESSAGE_INVALID_USER_INPUT_EXCEPTION = 'A user-supplied input was invalid.';

export let InvalidUserInputException = class InvalidUserInputException extends Error {
    constructor(...args) {
        super(...args);
        this.message = args[0] || MESSAGE_INVALID_USER_INPUT_EXCEPTION;
    }
};