import numeral from 'numeral';
import 'numeral/en-gb';

const DEFAULT_CURRENCY_FORMAT = '$0,0.00';
const DEFAULT_CURRENCY_NO_PENCE_FORMAT = '$0,0[.]00';

export function getNumeral(value, format = null) {
    numeral.locale('en-gb');

    if (format != null) {
        return numeral(value).format(format);
    }

    return numeral(value).value();
}

export function getIntegerFromStringCurrency(value) {
    if(value != null && value !== '') {
        numeral.locale('en-gb');
        return numeral(value).multiply(100).value();
    }

    return 0;
}

export function formattedCurrencyFromPenceAmount(value) {
    if(value != null) {
        if(value == 0) {
            return getNumeral(numeral(value).value(), DEFAULT_CURRENCY_FORMAT);
        }

        return getNumeral(numeral(value).divide(100).value(), DEFAULT_CURRENCY_FORMAT);
    }

    return '';
}

export function formattedCurrencyFromPoundsAmount(value) {
    if(value != null) {
        return getNumeral(numeral(value).value(), DEFAULT_CURRENCY_FORMAT);
    }

    return '';
}

export function roundedFormattedCurrencyFromPenceAmount(value) {
    if(value != null) {
        if(value == 0) {
            return getNumeral(numeral(value).value(), DEFAULT_CURRENCY_NO_PENCE_FORMAT);
        }

        return getNumeral(Math.round(numeral(value).divide(100).value()), DEFAULT_CURRENCY_NO_PENCE_FORMAT);
    }

    return '';
}

export function roundedFormattedCurrencyFromPoundsAmount(value) {
    if(value != null) {
        return getNumeral(Math.round(numeral(value).value()), DEFAULT_CURRENCY_NO_PENCE_FORMAT);
    }

    return '';
}
