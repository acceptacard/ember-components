export const GRAPH_GREEN = 'rgba(125,175,88, 0.75)';
export const GRAPH_RED = 'rgba(219,40,40, 0.75)';

export function hexToRgbA(hex, opacity = 1) {
    if (/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)) {
        let c = hex.substring(1).split('');

        if (c.length == 3) {
            c = [c[0], c[0], c[1], c[1], c[2], c[2]];
        }

        c = `0x${c.join('')}`;

        let rgba = `rgba(${[(c >> 16) & 255, (c >> 8) & 255, c & 255].join(',')},${opacity})`;

        return rgba;
    }

    return null;
}

export const COLOUR_GREEN = 'green';
export const COLOUR_YELLOW = 'yellow';
export const COLOUR_RED = 'red';