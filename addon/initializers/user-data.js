export function initialize(application) {
    application.inject('route', 'userData', 'service:user-data');
    application.inject('controller', 'userData', 'service:user-data');
}

export default {
    initialize
};
