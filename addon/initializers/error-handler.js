export function initialize(application) {
    application.inject('route', 'errorHandler', 'service:error-handler');
    application.inject('controller', 'errorHandler', 'service:error-handler');
}

export default {
    initialize
};
