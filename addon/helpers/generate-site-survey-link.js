import Helper from '@ember/component/helper';
import {getOwner} from '@ember/application';

export default Helper.extend({
    compute([isIso, eak]) {
        const ENV = getOwner(this).resolveRegistration('config:environment');
        let signupUrl = `${ENV.apiHost}/signup/common/site_survey?eak=${eak}&`;

        if(isIso) {
            signupUrl += 'application_type=iso';
        } else {
            signupUrl += 'application_type=submerchant';
        }

        return signupUrl;
    }
});