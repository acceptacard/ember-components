import {helper} from '@ember/component/helper';

export function generateDocumentDownloadLink([url, id, jwt]) {
    return `${url}/${id}?token=${jwt}`;
}

export default helper(generateDocumentDownloadLink);
