import {helper} from '@ember/component/helper';
import {getNumeral} from 'ember-components/modules/numeral';

export function toNumeral([value, format = null]) {
    return getNumeral(value, format);
}

export default helper(toNumeral);
