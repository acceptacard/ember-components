import {helper} from '@ember/component/helper';

export function formatSortCode([str]) {
    return formatSortCode(str);
}

export default helper(formatSortCode);
