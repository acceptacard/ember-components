import Helper from '@ember/component/helper';
import {replaceString} from 'ember-components/modules/string';

export default Helper.extend({
    compute([originalString, stringToReplace, replacementString]) {
        return replaceString(originalString, stringToReplace, replacementString);
    }
});