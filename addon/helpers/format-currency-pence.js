import {helper} from '@ember/component/helper';
import {formattedCurrencyFromPenceAmount} from 'ember-components/modules/numeral';

export function formatCurrencyPence([pence]) {
    if(pence != null) {
        return formattedCurrencyFromPenceAmount(pence);
    }
}

export default helper(formatCurrencyPence);
