import { helper } from '@ember/component/helper';
import {DEFAULT_DATE_FORMAT, DEFAULT_TIME_FORMAT} from 'ember-components/modules/datetime';
import moment from 'moment';

export function createdAtUpdatedAtText([createdAt, updatedAt = null]) {
    createdAt = moment(createdAt);
    let text = `Created on ${createdAt.format(DEFAULT_DATE_FORMAT)} at ${createdAt.format(DEFAULT_TIME_FORMAT)}`;

    if(updatedAt) {
        updatedAt = moment(updatedAt);
        let today = moment();
        let diffMs = updatedAt.diff(today);
        text += ` and updated ${moment.duration(diffMs).humanize(true)}`;
    }

    return text;
}

export default helper(createdAtUpdatedAtText);
