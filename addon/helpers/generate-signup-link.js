import Helper from '@ember/component/helper';
import {getOwner} from '@ember/application';

const PARTNER_TYPE_DIRECT = 'direct';
const ACQUIRER_ELAVON_URL_CODE = 'a4';
const BLACK_CAB_PARTNERS = [
    'BLACKCABBUYCP', 'BLACKCABCP', 'BLACKCABMONTHLYCP', 'BLACKCABTFLBUYCP', 'BLACKCABTFLCP', 'BLACKCABTFLMONTHLYCP',
    'BLKCABMONTHLYLOWCP', 'BLKCABTFLMTHLOWCP', 'PAYATAXICPS'
];

export default Helper.extend({
    compute([partner, referralCode, isSales, eak = null, review = false]) {
        const ENV = getOwner(this).resolveRegistration('config:environment');
        let signupUrl = `${ENV.apiHost}/signup/`;

        if(eak) {
            if (partner.get('iso')) {
                if(partner.get('charity')) {
                    signupUrl += 'pac/';
                } else {
                    signupUrl += `${ACQUIRER_ELAVON_URL_CODE}/`;
                }
            } else if (BLACK_CAB_PARTNERS.includes(partner.get('id'))) {
                signupUrl += 'bc/';
            }

            if(review) {
                signupUrl += 'review';
            } else {
                signupUrl += 'start';
            }

            signupUrl += `?eak=${eak}&`;
        } else {
            signupUrl = `${ENV.apiHost}/signup/quote?`;

            if(partner.get('id')) {
                signupUrl += `partner=${partner.get('id')}&`;
            }

            if(referralCode) {
                signupUrl += `referral_code=${referralCode}&`;
            }
        }

        if(!partner.get('iso') && partner.get('partnerType') == PARTNER_TYPE_DIRECT) {
            signupUrl += `merchant_type=${partner.get('partnerType')}&`;
        }

        if(partner.get('iso')) {
            signupUrl += 'origin=PCS&application_type=iso&';
        } else {
            signupUrl += 'application_type=submerchant&';
        }

        if(isSales) {
            signupUrl += `registration_type=TS`;
        }

        if(['&', '?'].includes(signupUrl.substr(-1))) {
            signupUrl = signupUrl.substr(0, signupUrl.length - 1);
        }

        return signupUrl;
    }
});