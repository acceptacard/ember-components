import {helper} from '@ember/component/helper';

const YES = 'Yes';
const NO = 'No';

export function yesNo(params) {
    if (['y', "1", 1, true].includes(params[0])) {
        return YES;
    } else {
        return NO;
    }
}

export default helper(yesNo);
