import Component from '@ember/component';
import {inject} from '@ember/service';
import {computed} from '@ember/object';
import layout from '../templates/components/sui-datagrid-controls';

export default Component.extend({
    layout,
    media: inject(),
    tagName: '',

    refreshingLoader: computed('datagridLoading', 'refreshingResults', function() {
        if(!this.get('media.isDesktop')) {
            if(this.get('datagridLoading') && this.get('refreshingResults')) {
                return true;
            } else {
                this.set('refreshingResults', false);
                return false;
            }
        }
    }),

    fromDateLoader: computed('datagridLoading', 'changingFromDate', function() {
        if(this.get('datagridLoading') && this.get('changingFromDate')) {
            return true;
        } else {
            this.set('changingFromDate', false);
            return false;
        }
    }),

    toDateLoader: computed('datagridLoading', 'changingToDate', function() {
        if(this.get('datagridLoading') && this.get('changingToDate')) {
            return true;
        } else {
            this.set('changingToDate', false);
            return false;
        }
    }),

    actions: {
        showHideFilters() {
            this.get('actionShowHideFilters')();
        },

        changeFromDate(dates, dateString) {
            if(this.get('media.isMobile')) {
                this.set('changingFromDate', true);
            }

            this.get('actionFromDateChange')(dates, dateString);
        },

        changeToDate(dates, dateString) {
            if(this.get('media.isMobile')) {
                this.set('changingToDate', true);
            }

            this.get('actionToDateChange')(dates, dateString);
        },

        refresh() {
            if(this.get('media.isMobile')) {
                this.set('refreshingResults', true);
            }

            this.get('actionRefreshList')();
        },

        resetFilters() {
            this.get('actionResetList')();
        },

        requestCsvReport() {
            this.get('actionRequestCsvReport')();
        }
    }
});