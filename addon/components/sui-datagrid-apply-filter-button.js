import Component from '@ember/component';
import {inject} from '@ember/service';
import {computed} from '@ember/object';
import layout from '../templates/components/sui-datagrid-apply-filter-button';

export default Component.extend({
    layout,
    media: inject(),

    tagName: '',

    filteringLoader: computed('datagridLoading', 'filteringResults', function() {
        if(this.get('datagridLoading') && this.get('filteringResults')) {
            return true;
        } else {
            this.set('filteringResults', false);
            return false;
        }
    }),

    actions: {
        filterResults() {
            if(this.get('media.isMobile')) {
                this.set('filteringResults', true);
            }

            this.get('actionFilterResults')();
        },
    }
});
