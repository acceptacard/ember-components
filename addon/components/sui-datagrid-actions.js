import Component from '@ember/component';
import layout from '../templates/components/sui-datagrid-actions';

export default Component.extend({
  layout,
  classNames: ['ui', 'tiny', 'secondary', 'menu']
});
