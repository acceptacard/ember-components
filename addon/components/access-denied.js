import Component from '@ember/component';
import layout from '../templates/components/access-denied';
import {inject} from '@ember/service';

export default Component.extend({
    layout,
    media: inject(),
    classNames: ['ui', 'centered', 'padded', 'grid'],

    actions: {
        back() {
            if(window.history.length > 1) {
                window.history.back();
            } else {
                this.transitionTo('index');
            }
        }
    }
});
