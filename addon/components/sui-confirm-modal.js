import Component from '@ember/component';
import layout from '../templates/components/sui-confirm-modal';

export default Component.extend({
    layout,
    tagName: '',
});
