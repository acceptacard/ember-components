import Component from '@ember/component';
import {task} from 'ember-concurrency';
import {inject} from '@ember/service';
import {computed} from '@ember/object';
import { IN_FUTURE } from 'ember-components/modules/datepickers';
import { Y, N } from 'ember-components/modules/string';
import layout from '../templates/components/notes-view';

export default Component.extend({
    layout,
    store: inject(),
    errorHandler: inject(),
    notifications: inject(),

    showAddNote: false,
    newNote: null,
    newNoteActionRequired: false,

    actionRequiredByDatepickerOptions: IN_FUTURE,

    liveNotes: computed('notes.[]', function() {
        return this.get('store').peekAll('note');
    }),

    filteredNotes: computed('liveNotes.[]', 'filterText', function() {
        return this.get('liveNotes').filter((note) => {
            let currentTrader = true;

            if(note.get('traderKey') != this.get('merchantId') || note.get('isNew')) {
                currentTrader = false;
            }

            if(currentTrader && this.get('filterText')) {
                if(note.get('notes').toLowerCase().includes(this.get('filterText').toLowerCase())
                    || note.get('operator').toLowerCase().includes(this.get('filterText').toLowerCase())) {
                    currentTrader = true;
                } else {
                    currentTrader = false;
                }
            }

            return currentTrader;
        }).sortBy('notesDateTime').reverse();
    }),

    filterText: '',

    taskSaveNote: task(function * () {
        try {
            this.set('newNote.traderKey', this.get('merchantId'));

            if(this.get('newNoteActionRequired')) {
                this.set('newNote.actionRequired', Y);
            } else {
                this.set('newNote.actionRequired', N);
            }

            yield this.get('newNote').save();

            this.setProperties({
                newNote: null,
                newNoteActionRequired: false
            });

            this.send('addNote');
            this.get('notifications').success('Note saved successfully.');
        } catch(e) {
            this.get('errorHandler').handleErrors(e);
        }
    }).drop(),

    actions: {
        filterTextChanged(filterText) {
            this.set('filterText', filterText);
        },

        addNote() {
            this.toggleProperty('showAddNote');
            if(!this.get('newNote')) {
                this.set('newNote', this.get('store').createRecord('note', {
                    actionRequiredDate: IN_FUTURE.minDate
                }));
            }
        },

        clearNote() {
            this.get('newNote').deleteRecord();
            this.setProperties({
                newNote: null,
                newNoteActionRequired: false
            });

            this.send('addNote');
        },

        saveNote() {
            this.get('taskSaveNote').perform();
        }
    }
});
