import Component from '@ember/component';
import layout from '../templates/components/sui-error-message';

export default Component.extend({
    layout,
    classNames: ['ui', 'red', 'message'],

    icon: 'warning',
    attribute: 'message'
});
