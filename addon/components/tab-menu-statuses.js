import Component from '@ember/component';
import layout from '../templates/components/tab-menu-statuses';

export default Component.extend({
    layout,
    tagName: ''
});
