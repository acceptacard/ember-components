import Component from '@ember/component';
import layout from '../templates/components/sui-progress-bar';
import $ from 'jquery';

export default Component.extend({
    layout,
    tagName: '',

    didInsertElement() {
        this._super(...arguments);
        $('#sui-progress-bar').progress({
            percent: 0
        });
    },

    didUpdateAttrs() {
        this._super(...arguments);
        $('#sui-progress-bar').progress('set percent', this.get('percent'));
    }
});
