import Component from '@ember/component';
import layout from '../templates/components/sui-icon-header';

export default Component.extend({
    layout,
    classNames: ['ui', 'header'],

    didReceiveAtts() {
        this.set('tagName', this.get('tagName'));
    }
});
