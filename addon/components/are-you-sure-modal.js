import Component from '@ember/component';
import layout from '../templates/components/are-you-sure-modal';

export default Component.extend({
  layout,
  tagName: ''
});
